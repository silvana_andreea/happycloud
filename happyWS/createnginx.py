#!/usr/bin/env python3

#import modules
import boto3
dryRun = False; # useful variable to put the script into dry run mode where the function allows it

# Definitions
client = boto3.client(
    's3',
    aws_access_key_id='AKIATQ35XHGYLWCI5Q4Z',
    aws_secret_access_key='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'
)

ec2Client = boto3.client('ec2', region_name='us-east-1')
ec2Resource = boto3.resource('ec2', region_name='us-east-1')
route53Client = boto3.client('route53', region_name='us-east-1')


# Create instance
myinstance = ec2Resource.create_instances(
            ImageId='ami-0323c3dd2da7fb37d',
            InstanceType='t2.micro',
            SecurityGroupIds=['sg-0e191c32f1516c319'],
            SubnetId='subnet-0a1786ce46703dca5',
            KeyName='HappyCloudJenkinsKey',
                MinCount=1,
                MaxCount=1,
                UserData='''#!/bin/bash
                amazon-linux-extras install -y nginx1
                yum -y install nginx
                echo "<html><marquee direction="up"><h1>Congratulations Steve! Your WebServer is up</h1></marquee></html>" >/usr/share/nginx/html/index.html
                systemctl enable nginx
                systemctl start nginx''')

# Tag instance
for instance in myinstance:
    print("Instance ID: " + instance.id)
    ec2Resource.create_tags(Resources = [instance.id], Tags = [
        {'Key':'Name', 'Value':'NGINX'},
        {'Key':'Project', 'Value':'assessment2'},
        {'Key':'Owner', 'Value':'HappyCloud'}
        ])

# Wait for it to launch before assigning the elastic IP address
myinstance[0].wait_until_running();

# Allocate an elastic IP
eip = ec2Client.allocate_address(Domain='vpc')

# Associate the elastic IP address with the instance launched above
ec2Client.associate_address(
     InstanceId = myinstance[0].id,
     AllocationId = eip["AllocationId"])
