#!/usr/bin/env python3

import boto3
import sys

ec2=boto3.client('ec2',region_name='us-east-1')

# Check if security group exists
try:
    sgdata=ec2.describe_security_groups(
        GroupNames='PHPFirewall'
    )

except:
    # Create Security Group
    secgrp = ec2.create_security_group(
            GroupName='PHPFirewall',
            Description='HappyClouds PHP Firewall',
            VpcId='vpc-08c8a23fbf29fc069'

    )
    print("Security Group "+str(secgrp['GroupId']))

    fwrules=ec2.authorize_security_group_ingress(
            GroupId=secgrp['GroupId'],
            IpPermissions=[
                {
                    'IpProtocol': 'tcp',
                    'FromPort': 22,
                    'ToPort': 22,
                    'IpRanges': [{'CidrIp': '86.47.40.12/32'}, {'CidrIp': '82.24.141.235/32'}, {'CidrIp': '188.24.217.40/32'}]
                },
                {
                    'IpProtocol': 'tcp',
                    'FromPort': 80,
                    'ToPort': 80,
                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
                },
                {
                    'IpProtocol': '-1',
                    'IpRanges': [{'CidrIp': '172.30.0.0/16'}]
                },
                {
                    'IpProtocol': 'tcp',
                    'FromPort': 443,
                    'ToPort': 443,
                    'IpRanges': [{'CidrIp': '0.0.0.0/0'}]
                }
            ]
    )
    print("Firewall rules applied to security group")

    # Get Security Group Detail
    sgdata=ec2.describe_security_groups(
            GroupIds=[secgrp['GroupId']]
    )

    print(sgdata)
