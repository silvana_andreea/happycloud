# README #

### happyCloud ###
happyCloud Assessment 2 repository

### Team members: ###
  * David
  * Fani
  * Silviana

### Tools Used ###
  * Editor for developing -> Atom (languages Python & Shell, plug-in Git);
  * Communication -> Slack; Google Meet
  * Project Management & Planning -> Trello:
  [https://trello.com/b/W3dOVIbE/happy-cloud]
  [Project Plan -Google Docs]
  [Schematic diagram of network -Google Drive]
  * Source/Version Control -> Bitbucket & Git;
  * Automante and config virtual environments -> Amazon AWS

### Instructions ###
  * Run 'main.py' script on your terminal in order to create the network
      infrastructure and to create the Jenkins environment
  * Login into Jenkins env using the 'Login Jenkins Details' written below
  * Go on Amazon AWS (profile = academy, region = us-east-1) in order to
      see the items created, available and/or running
  * Login into Jenkins env to run the Jenkins Jobs that create/run the
      applications: MySQL and WebServers, PHP and PetClinic

### Login Jenkins Details ###
  * Jenkins URL: http://54.158.91.157:8080/
  * username: happyUser
  * password: academyapr2020
  * full name: HappyCloud Team
  * e-mail address: silviana-andreea@automationlogic.com


### Reference of external doc created ###
  * Create a schematic diagram of our network -
      https://drive.google.com/file/d/1ak19C-NXwMsLg0QDd4bAEgz_dyl9qEAT/view?usp=sharing
  * Create a project plan -
      https://docs.google.com/document/d/1-FghMZxfqHfA0o2LKq3SWF9wk8CpWmT6R91vvHrbhaQ/edit?usp=sharing
