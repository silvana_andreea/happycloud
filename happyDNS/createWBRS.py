#!/usr/bin/env python3

import boto3
import sys

# Definitions
client = boto3.client('route53', region_name='us-east-1')

zones = client.list_hosted_zones_by_name()
if not zones or len(zones['HostedZones']) == 0:
    raise Exception("Could not find DNS zone to update")
for zone in zones['HostedZones']:
    print(zone)
    if zone['Name'] == 'happycloud.academy.grads.al-labs.co.uk.':
        zone_id = zone['Id']
        print("Zone ID: "+zone_id)
        #Create record set
        try:
            response = client.change_resource_record_sets(
            HostedZoneId=zone_id,
            ChangeBatch= {
                    'Comment': 'Adding DNS for WebServer',
                    'Changes': [
                    {
                        'Action': 'CREATE',
                        'ResourceRecordSet': {
                        'Name': "nginx.happycloud.academy.grads.al-labs.co.uk",
                        'Type': 'CNAME',
                        'SetIdentifier': "Web Server DNS",
                        'TTL': 60,
                        'Region': 'us-east-1',
                        'ResourceRecords': [{'Value': 'WebserverELB-1733145928.us-east-1.elb.amazonaws.com'}]
                    }
                }]
            })

            print(response)

        except Exception as e:
        	print(e)
