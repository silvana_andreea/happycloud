
# Allocate an elastic IP
eip = ec2Client.allocate_address(Domain='vpc')

# Associate the elastic IP address with the instance launched above
ec2Client.associate_address(
     InstanceId = myinstance[0].id,
     AllocationId = eip["AllocationId"])

# Check if zone already exists
zones = route53Client.list_hosted_zones_by_name()
if not zones or len(zones['HostedZones']) == 0:
    raise Exception("Could not find DNS zone to update")
for zone in zones['HostedZones']:
    print(zone)

    # Find zone ID
    if zone['Name'] == 'happycloud.academy.grads.al-labs.co.uk.':
        zone_id = zone['Id']
        print("Zone ID: "+zone_id)

        # Add record set
        try:
            response = route53Client.change_resource_record_sets(
            HostedZoneId=zone_id,
            ChangeBatch= {
                    'Comment': 'Adding DNS for WebServer',
                    'Changes': [
                    {
                        'Action': 'CREATE',
                        'ResourceRecordSet': {
                        'Name': "nginx.happycloud.academy.grads.al-labs.co.uk",
                        'Type': 'A', #CNAME
                        'SetIdentifier': "Web Server DNS",
                        'TTL': 10,
                        'Region': 'us-east-1',
                        'ResourceRecords': [{'Value': eip["PublicIp"]}] #LB DNS name
                    }
                }]
            })

            print(response)

        except Exception as e:
        	print(e)
