#!/usr/bin/env python3

import boto3

region='us-west-2'

ec2=boto3.client('ec2',region_name=region)
rds=boto3.client('rds',region_name=region)

subnets= ec2.describe_subnets(Filters=[{'Name':'tag:Name','Values':['priv1','priv2']}])
priv1=subnets['Subnets'][0]['SubnetId']
priv2=subnets['Subnets'][1]['SubnetId']
privsubs=[priv1,priv2]

secgrps=ec2.describe_security_groups(Filters=[{'Name':'group-name','Values':['HappyDBsecgrp']}])
vpcsecgrp=secgrps['SecurityGroups'][0]['GroupId']

###----- Tags -----###
db_subnet_tags=[{"Key": "Name", "Value": "happyDBSubnet"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy_Group"}]
db_tags=[{"Key": "Name", "Value": "happyDB"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy_Group"}]

###--- Create a subnet group ---###
dbsubnet=rds.create_db_subnet_group(
    DBSubnetGroupName='happy_sub',
    DBSubnetGroupDescription='happy_db_subnet',
    SubnetIds=privsubs,
    Tags=db_subnet_tags
)

###--- Create Database Instance ---###
dbinstance=rds.create_db_instance(
    ###----- DB Instance Parameters -----###
    DBInstanceIdentifier='happydbID',
    AllocatedStorage=20,
    DBInstanceClass='db.t2.micro',
    Engine='mariadb',
    EngineVersion='10.2.21',
    MasterUsername='petclinc',
    MasterUserPassword='petclinic',
    MultiAZ=True,
    PubliclyAccessible=False,
    Port=3306,
    DBSubnetGroupName='happy_sub',
    VpcSecurityGroupIds=[vpcsecgrp],
    Tags=db_tags,

    ###----- DB Parameters ------###
    DBName= 'petclinic',

    ###----- DB Backup ------###
    BackupRetentionPeriod=4,
    PreferredBackupWindow='21:00-23:00', # Must be:  hh:mi-hh:mi
    CopyTagsToSnapshot=True,

    ###----- Amazon Updates -----###
    PreferredMaintenanceWindow='sun:23:01-sun:23:31') # Must be: ddd:hh24:mi-ddd:hh24:mi
