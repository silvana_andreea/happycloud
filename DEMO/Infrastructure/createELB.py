#!/usr/bin/env python3

import boto3

def create_elb(elb,lbname,sub1,sub2,secgrp):

    ###--- Create PHP Elastic Load Balancer ---###
    response=elb.create_load_balancer(
            LoadBalancerName=lbname,
            Listeners=[
            {
                'Protocol': 'HTTP',
                'LoadBalancerPort': 80,
                'InstanceProtocol': 'HTTP',
                'InstancePort': 80
            }
            ],
            Subnets=[sub1,sub2],
            SecurityGroups=[secgrp],
            Tags=[
                {
                    'Key': 'Name',
                    'Value': lbname
                }
            ]
    )
    return
