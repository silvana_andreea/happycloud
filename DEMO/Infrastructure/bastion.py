#!/usr/bin/env python3

###----- This script provisions a bastion vm to have SSH access to the DB server on the private subnet -----###

import boto3

def create_bastion(ec2,secgrp,pubsub1,pubsub2,keyname):
    provfile='''#!/bin/bash
    sudo yum -y install mysql'''

    ###--- Create Bastions for Each Region ---###
    bastion1 = ec2.create_instances(
    	# ImageId = 'ami-0323c3dd2da7fb37d', #N.Virginia
        ImageId = 'ami-0d6621c01e8c2de2c', #Oregon
    	InstanceType = 't2.micro',
    	SecurityGroupIds = [secgrp],
    	KeyName = keyname,
        SubnetId = pubsub1,
        MinCount = 1,
        MaxCount = 1,
        UserData=provfile)

    bastion2 = ec2.create_instances(
    	# ImageId = 'ami-0323c3dd2da7fb37d', #N.Virginia
        ImageId = 'ami-0d6621c01e8c2de2c', #Oregon
    	InstanceType = 't2.micro',
    	SecurityGroupIds = [secgrp],
    	KeyName = keyname,
        SubnetId = pubsub2,
        MinCount = 1,
        MaxCount = 1,
        UserData=provfile)

    i=[bastion1,bastion2]
    count=0

    ###--- Tag Bastion Instances ---###
    for Instances in i:
    	count=count+1
    	for instance in Instances:
    	    print("Instance ID: " + instance.id)
    	    ec2.create_tags(Resources = [instance.id], Tags = [
    	        {'Key':'Name', 'Value':'Bastion'+str(count)},
    	        {'Key':'Project', 'Value':'ass_2'},
    	        {'Key':'Owner', 'Value':'Happy Group'}
    	        ])

    return
