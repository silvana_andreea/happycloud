#!/usr/bin/env python3

import boto3

def createJenkins(ec2,ec2extra,route53,sshkey,subnet,secgrp):

    # ami_id = 'ami-0323c3dd2da7fb37d' #N.Virginia
    ami_id = 'ami-0d6621c01e8c2de2c' #Oregon

    # Define function for creating instance
    myinstance = ec2.create_instances(
        ImageId = ami_id,
        InstanceType = 't2.large',
        SecurityGroupIds = [secgrp],
        SubnetId = subnet,
        KeyName = sshkey,
        MinCount = 1,
        MaxCount = 1,
    )

    # Tag instance
    for instance in myinstance:
        print("Instance ID: " + instance.id)
        jenkinsid=instance.id
        ec2.create_tags(Resources = [instance.id], Tags = [
            {'Key':'Name', 'Value':'Jenkins'},
            {'Key':'Project', 'Value':'Assessment 2'},
            {'Key':'Owner', 'Value':'HappyCloud Team'}
    ])

    # Wait for the instance to launch before assigning the EIP address
    myinstance[0].wait_until_running();

    ###--- Create Elastic IP ---###
    eip=ec2extra.allocate_address(Domain='vpc')

    ###--- Assign it to Jenkins Server ---###
    ec2extra.associate_address(
        InstanceId = jenkinsid,
        AllocationId = eip["AllocationId"])

    # # Add the route 53 record set to the hosted zone for the domain
    # route53.change_resource_record_sets(
    #         HostedZoneId = 'Z01144081QU3IR5HAOGGM',
    #         ChangeBatch= {
    #         'Comment': 'Add Jenkins server to Route53',
    #         'Changes': [
    #         {
    #             'Action': 'CREATE',
    #             'ResourceRecordSet':
    #             {
    #                 'Name': 'jenkins.happycloud.academy.grads.al-labs.co.uk.',
    #                 'Type': 'A',
    #                 'TTL': 10,
    #                 'ResourceRecords':
    #                 [
    #                 {
    #                     'Value': '54.158.91.157'
    #                 },
    #                 ],
    #             }
    #         },
    #     ]
    # })
    return
