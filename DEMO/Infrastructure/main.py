#!/usr/bin/env python3

import sys
import boto3
import create_vpc
import create_key
import DBsecgrp
import WSsecgrp
import PHPsecgrp
import PCsecgrp
import jenkinssecgrp
import bastion
import createELB
import create_jenkins

region = 'us-west-2'
az1='us-west-2a'
az2='us-west-2b'

ec2=boto3.resource('ec2',region_name=region)
ec2extra =boto3.client('ec2',region_name=region)
client=boto3.client('ec2',region_name=region)
rds=boto3.client('rds',region_name=region)
elb=boto3.client('elb',region_name=region)
route53=boto3.client('route53',region_name=region)

###--- Create VPC ---###
print("***--- Creating VPC ---***")
pubsub1,pubsub2,privsub1,privsub2,vpcID = create_vpc.createVPC(ec2,client,az1,az2)
print("Public1 Subnet ID: "+str(pubsub1))
print("Public2 Subnet ID: "+str(pubsub2))
print("Private1 Subnet ID: "+str(privsub1))
print("Private2 Subnet ID: "+str(privsub2))

###--- Create Security Groups ---###
print("***--- Creating Security Groups ---***")
DBgrpName="HappyDBsecgrp"
WSgrpName="HappyWSsecgrp"
PHPgrpName="HappyPHPsecgrp"
PCgrpName="HappyPCsecgrp"
jenkinsgrpName="JenkinsFirewall"
DBsecID=DBsecgrp.createsg(client,DBgrpName,vpcID)
WSsecID=WSsecgrp.createsg(client,WSgrpName,vpcID)
PHPsecID=PHPsecgrp.createsg(client,PHPgrpName,vpcID)
PCsecID=PCsecgrp.createsg(client,PCgrpName,vpcID)
jenkinssecID=jenkinssecgrp.createsg(client,jenkinsgrpName,vpcID)

###--- Create Bastion SSH Key Pair ---###
print("***--- Creating Bastion/Jenkins SSH Keys ---***")
bkeyname="bastion_key"
jkeyname="HappyCloudJenkinsKey"
bdescription='Bastion SSH Key for HappyGroup'
jdescription='Jenkins SSH Key for HappyGroup'
bkey=create_key.createkey(client,bkeyname,bdescription)
print('Bastion SSH KEY')
jenkinskey=create_key.createkey(client,jkeyname,jdescription)
print('Jenkins SSH KEY')

###--- Create Bastion ---###
print("***--- Creating Bastion ---***")
bastion.create_bastion(ec2,DBsecID,pubsub1,pubsub2,bkeyname)

###--- Create IAM Roles ---###
# print("***--- Creating IAM Roles ---***")


###--- Create Load Balancers ---###
print("***--- Creating Load Balancers ---***")
WS_elbname="WebserverELB"
PHP_elbname="PHPELB"
PC_elbname="PetclinicELB"
createELB.create_elb(elb,WS_elbname,pubsub1,pubsub2,WSsecID)
print(WS_elbname)
createELB.create_elb(elb,PHP_elbname,pubsub1,pubsub2,PHPsecID)
print(PHP_elbname)
createELB.create_elb(elb,PC_elbname,pubsub1,pubsub2,PCsecID)
print(PC_elbname)

###--- Create Instances ---###

# ###--- Register Web Server With Elastic Load Balancer ---###
# register_ELB.registerELB(elb,WS_elbname,instanceId)
# register_ELB.registerELB(elb,PHP_elbname,instanceId)
# register_ELB.registerELB(elb,PC_elbname,instanceId)

###--- Create Jenkins Server From AMI ---###
print("***--- Creating Jenkins Server ---***")
create_jenkins.createJenkins(ec2,ec2extra,route53,jkeyname,pubsub1,jenkinssecID)
