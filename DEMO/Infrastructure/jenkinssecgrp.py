#!/usr/bin/env python3

import boto3
import sys

def createsg(ec2,grpName,vpcid):

# Check if security group exists
    try:
        sgdata=ec2.describe_security_groups(GroupNames=[grpName])
    except:
        # Create Security Group
        secgrp=ec2.create_security_group(
                GroupName=grpName,
                Description=grpName+' Automated Python Security Group',
                VpcId=vpcid

        )
        print("Security Group "+str(secgrp['GroupId']))

        fwrules=ec2.authorize_security_group_ingress(
                GroupId=secgrp['GroupId'],
                IpPermissions=[
                    {
                        'IpProtocol': '-1',
                        'IpRanges': [{'CidrIp': '172.30.0.0/16'}]
                    },
                    {
                        'IpProtocol': '-1',
                        'IpRanges': [{'CidrIp': '86.47.40.12/32'}, {'CidrIp': '82.24.141.235/32'}, {'CidrIp': '188.24.217.40/32'}, {'CidrIp': '82.24.122.149/32'}]
                    }
                ]
        )
        print("Firewall rules applied to security group")

            ###--- Get Security Group Detail ---###
        sgdata=ec2.describe_security_groups(GroupIds=[secgrp['GroupId']])

        return secgrp['GroupId']
