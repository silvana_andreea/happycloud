#!/usr/bin/env python3

import boto3

asgclient = boto3.client('autoscaling', 'us-east-1')

# Create ASG

lbname="PetclinicELB"

# Launch Configuration
lc=asgclient.create_launch_configuration(
    LaunchConfigurationName="PCLC",
    ImageId="ami-06ce3edf0cff21f07",
    InstanceType='t2.micro',
    KeyName="steveshillingacademyie",
    AssociatePublicIpAddress=True
)

asg=asgclient.create_auto_scaling_group(
        AutoScalingGroupName='PetclinicASG',
        LaunchConfigurationName='PSLC',
        MinSize=1,
        MaxSize=3,
        DesiredCapacity=1,
        LoadBalancerNames="PetclinicELB",
        AvailabilityZones=['eu-west-1a','eu-west-1b','eu-west-1c']
    )

attach=asgclient.attach_load_balancers(
    AutoScalingGroupName='PetclinicASG',
    LoadBalancerNames=[lbname]
)
