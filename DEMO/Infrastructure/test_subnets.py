#!/usr/bin/env python3

import boto3

ec2=boto3.client('ec2',region_name='us-east-1')

secgrps=ec2.describe_security_groups(Filters=[{'Name':'group-name','Values':['HappyDBsecgrp']}])
DBsecgrp=secgrps['SecurityGroups'][0]['GroupId']
print(DBsecgrp)
