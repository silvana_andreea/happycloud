#!/usr/bin/env python3

#import modules
import boto3
dryRun = False; # useful variable to put the script into dry run mode where the function allows it

# Definitions
ec2Client = boto3.client('ec2', region_name='us-east-1')
ec2Resource = boto3.resource('ec2', region_name='us-east-1')
route53Client = boto3.client('route53', region_name='us-east-1')


# Create instance
myinstance = ec2Resource.create_instances(
            ImageId='ami-0915e09cc7ceee3ab',
            InstanceType='t2.micro',
            SecurityGroupIds=['sg-08e74600cb490610f'],
            SubnetId='subnet-0a1786ce46703dca5',
            KeyName='HappyCloudJenkinsKey',
                MinCount=1,
                MaxCount=1,
                UserData='''#!/bin/bash
                yum -y install httpd24 php56 php56-mysqlnd
                sudo service enable httpd
                sudo chkconfig httpd on
                sudo service start httpd''')

# Tag instance
for instance in myinstance:
    print("Instance ID: " + instance.id)
    ec2Resource.create_tags(Resources = [instance.id], Tags = [
        {'Key':'Name', 'Value':'PHP'},
        {'Key':'Project', 'Value':'assessment2'},
        {'Key':'Owner', 'Value':'HappyCloud'}
        ])

# Wait for it to launch before assigning the elastic IP address
myinstance[0].wait_until_running();

# Allocate an elastic IP
eip = ec2Client.allocate_address(Domain='vpc')

# Associate the elastic IP address with the instance launched above
ec2Client.associate_address(
     InstanceId = myinstance[0].id,
     AllocationId = eip["AllocationId"])
