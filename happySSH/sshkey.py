#!/usr/bin/env python3

import boto3
import os
import stat

ec2=boto3.client('ec2',region_name='us-east-1')

# Check if Key exists
newkey=0
try:
    sshkey=ec2.describe_key_pairs(
            KeyNames = 'HappyCloudJenkinsKey'
    )
except:
    newkey=1

if newkey == 1:
    # Create SSH Key
    sshkey=ec2.create_key_pair(
            KeyName='HappyCloudJenkinsKey',
            TagSpecifications=[
                {
                    'ResourceType': 'key-pair',
                    'Tags': [
                        {
                            'Key': 'Name',
                            'Value': 'HappyCloudJenkinsKey'
                        }
                    ]
                }
            ]
    )
    print("SSH Key created in AWS")

    # Save the SSH Key
    filename='HappyCloudJenkinsKey'+".pem"
    sshfh = open(filename, "w")
    sshfh.write(sshkey['KeyMaterial'])
    sshfh.close()
    os.chmod(filename, stat.S_IRWXU )
    chkfh=open(filename,"r")
    chkdata=chkfh.read()
    chkfh.close()
    print("SSH Key saved to disk")
