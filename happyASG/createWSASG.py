#!/usr/bin/env python3

import boto3

asgclient = boto3.client('autoscaling', 'us-east-1')

lbname="WebserverELB"

# Launch Configuration
lc=asgclient.create_launch_configuration(
    LaunchConfigurationName="WSLC",
    ImageId="ami-0323c3dd2da7fb37d",
    InstanceType='t2.micro',
    KeyName="HappyCloudJenkinsKey",
    AssociatePublicIpAddress=True
)
# Create ASG
asg=asgclient.create_auto_scaling_group(
        AutoScalingGroupName='WebserverASG',
        LaunchConfigurationName='WSLC',
        MinSize=1,
        MaxSize=2,
        DesiredCapacity=1,
        LoadBalancerNames=[lbname],
        AvailabilityZones=['us-east-1a','us-east-1b']
    )
# Attach ASG to loadbalancer
attach=asgclient.attach_load_balancers(
    AutoScalingGroupName='WebserverASG',
    LoadBalancerNames=[lbname]
)
