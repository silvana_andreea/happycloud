#!/usr/bin/env python3

import sys
import boto3
import create_vpc
import db
import secgrp
import bastion
import bastion_key

ec2=boto3.resource('ec2',region_name='us-west-2')
client=boto3.client('ec2',region_name='us-west-2')
rds=boto3.client('rds',region_name='us-west-2')

###--- Create VPC ---###
print("Creating VPC")
pubsub1,pubsub2,privsub1,privsub2,vpcID = create_vpc.createVPC(ec2,client)
print("Public1 Subnet ID: "+str(pubsub1))
print("Public2 Subnet ID: "+str(pubsub2))
print("Private1 Subnet ID: "+str(privsub1))
print("Private2 Subnet ID: "+str(privsub2))

###--- Create Security Group ---###
print("Creating Security Group")
grpName="HappyDBgrp"
secID=secgrp.createsg(client,grpName,vpcID)
print("Security Group Created")

###--- Create Database Server ---###
print("Creating Database Server")
db.createDB(rds,privsub1,privsub2,secID)
print("Database Server Created")

###--- Create Bastion SSH Key Pair ---###
print("Creating Bastion SSH Key")
keyname="bastion_key"
description='Bastion SSH Key for HappyGroup'
sshkey=bastion_key.createkey(client,keyname,description)
print("Bastion SSH Key Created")

###--- Create Bastion ---###
print("Creating Bastion")
bastion.create_bastion(ec2,secID,pubsub1,pubsub2,keyname)
print("Bastion Created")
