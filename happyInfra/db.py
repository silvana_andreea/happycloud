#!/usr/bin/env python3

import boto3
import sys

    # def db_instance(rds,dbname,owner,sshkey,sgname):

def createDB(rds,sub1,sub2,sgdata):
    subnets=[sub1,sub2]
    vpcsecgrp=[sgdata]

    ###----- Tags -----###
    db_subnet_tags=[{"Key": "Name", "Value": "happyDBSubnet"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy_Group"}]
    db_tags=[{"Key": "Name", "Value": "happyDB"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy_Group"}]

    ###--- Create a subnet group ---###
    dbsubnet=rds.create_db_subnet_group(
        DBSubnetGroupName='happy_sub',
        DBSubnetGroupDescription='happy_db_subnet',
        SubnetIds=subnets,
        Tags=db_subnet_tags
    )

    ###--- Create Database Instance ---###
    dbinstance=rds.create_db_instance(
        ###----- DB Instance Parameters -----###
        DBInstanceIdentifier='happydbID',
        AllocatedStorage=20,
        DBInstanceClass='db.t2.micro',
        Engine='mariadb',
        EngineVersion='10.2.21',
        MasterUsername='root',
        MasterUserPassword='secret123',
        MultiAZ=True,
        PubliclyAccessible=False,
        Port=3306,
        DBSubnetGroupName='happy_sub',
        VpcSecurityGroupIds=vpcsecgrp,
        Tags=db_tags,

        ###----- DB Parameters ------###
        DBName= 'Petclinic',

        ###----- DB Backup ------###
        BackupRetentionPeriod=4,
        PreferredBackupWindow='21:00-23:00', # Must be:  hh:mi-hh:mi
        CopyTagsToSnapshot=True,

        ###----- Amazon Updates -----###
        PreferredMaintenanceWindow='sun:23:01-sun:23:31') # Must be: ddd:hh24:mi-ddd:hh24:mi

    return
