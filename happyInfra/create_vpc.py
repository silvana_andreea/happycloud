#!/usr/bin/env python3

###--- Script to create a VPC with public and private subnets with an IGW -----###
###--- The public will consist of an NAT and Bastion, the private a DB -----###

import boto3
import datetime

def createVPC(ec2,client):
    now=datetime.datetime.now()

    if now.day < 10:
      theday='0'+str(now.day)
    else:
      theday=str(now.day)

    mytags=[{"Key": "Name", "Value": "happyVPC"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy Group"}]
    pub1tag=[{"Key": "Name", "Value": "pub1"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy Group"}]
    pub2tag=[{"Key": "Name", "Value": "pub2"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy Group"}]
    priv1tag=[{"Key": "Name", "Value": "priv1"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy Group"}]
    priv2tag=[{"Key": "Name", "Value": "priv2"},{"Key": "Project","Value": "ass_2"},{"Key": "Owner", "Value": "Happy Group"}]

    ###--- Create VPC -----###
    vpc=ec2.create_vpc(CidrBlock='172.30.0.0/16')
    vpc.create_tags(Tags=mytags)
    vpc.wait_until_available()

    ###--- Create IGW (Internet Gateway) -----###
    internetgateway = ec2.create_internet_gateway()
    internetgateway.create_tags(Tags=mytags)
    vpc.attach_internet_gateway(InternetGatewayId=internetgateway.id)

    ###--- Create Public Routing Table & Public Route -----###
    pubroutetable = vpc.create_route_table()
    pubroutetable.create_tags(Tags=mytags)

    pubroute = pubroutetable.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=internetgateway.id)

    ###--- Create Public Subnet -----###
    pubsub1 = ec2.create_subnet(CidrBlock='172.30.1.0/24', VpcId=vpc.id, AvailabilityZone='us-west-2a')
    pubsub1.create_tags(Tags=pub1tag)
    psub1=client.modify_subnet_attribute(SubnetId=pubsub1.id,MapPublicIpOnLaunch={'Value': True})

    pubsub2 = ec2.create_subnet(CidrBlock='172.30.10.0/24', VpcId=vpc.id, AvailabilityZone='us-west-2b')
    pubsub2.create_tags(Tags=pub2tag)
    psub2=client.modify_subnet_attribute(SubnetId=pubsub2.id,MapPublicIpOnLaunch={'Value': True})

    ###--- Create Private Subnet -----###
    privsub1 = ec2.create_subnet(CidrBlock='172.30.2.0/24', VpcId=vpc.id, AvailabilityZone='us-west-2a')
    privsub1.create_tags(Tags=priv1tag)

    privsub2 = ec2.create_subnet(CidrBlock='172.30.20.0/24', VpcId=vpc.id, AvailabilityZone='us-west-2b')
    privsub2.create_tags(Tags=priv2tag)

    ###--- Associate Public Subnets With Public Routing Table -----###
    pubroutetable.associate_with_subnet(SubnetId=pubsub1.id)
    pubroutetable.associate_with_subnet(SubnetId=pubsub2.id)

    ###--- Create Private Routing Table & Private Route With NAT -----####
    privroutetable = vpc.create_route_table()
    privroutetable.create_tags(Tags=mytags)

    ###--- Associate Private Subnets With Private Routing Table -----###
    privroutetable.associate_with_subnet(SubnetId=privsub1.id)
    privroutetable.associate_with_subnet(SubnetId=privsub2.id)

    ###--- Return Subnet ID's ---###
    return pubsub1.id,pubsub2.id,privsub1.id,privsub2.id,vpc.id
